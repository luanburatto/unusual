﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLife : MonoBehaviour
{

    public int Maxlife;

    public int ActualLife;
    // Start is called before the first frame update
    void Start()
    {
        ActualLife = Maxlife;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ReceiveDamage()
    {
        ActualLife -= 1;

        if(ActualLife <=0)
        {
            Debug.Log("Game Over");
        }
    }
    
}
